package ru.bokhan.tm.exception;

public class EmptyNameException extends RuntimeException{

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}