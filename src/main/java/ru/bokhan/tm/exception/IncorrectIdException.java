package ru.bokhan.tm.exception;

public class IncorrectIdException extends RuntimeException{

    public IncorrectIdException() {
        super("Error! Id is incorrect...");
    }

}