package ru.bokhan.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showArguments();

    void showCommands();

    void showHelp();

    void exit();

}
